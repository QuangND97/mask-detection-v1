from retina_face.detect import detect_face, get_model, init
from imutils.video import VideoStream
from model.model import Classifier
from PIL import Image
from utils import *
import numpy as np
import argparse
import logging
import imutils
import torch
import time
import cv2
import os


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
buffer = None


def predict_mask(model, faces, preprocess_face, frame_idx, save_box=False):
	"""
	Return list of scores given list face
	Args:
		preprocess_face: pre-process standard
		model: classifier
		faces: ndarray
	"""
	global buffer
	score_nomask = []
	face_idx = 0

	for face in faces:
		face, image_path = cv2toPIL(buffer, face, frame_idx, face_idx)
		face = preprocess_face(face)
		prediction = model(face.unsqueeze(0))              # prediction shape: (2,)
		
		m = torch.nn.Softmax()
		score = m(prediction)[0]
		score_nomask.append(score)
		face_idx += 1

		# decide save box or not
		if save_box == False:
			cleanup(image_path)

	return score_nomask


def main():
	# load args and create buffer folder
	args = init()
	global buffer
	buffer = args.buffer
	maybe_create_folder(args.buffer)

	# load the face mask detector model from disk
	logger.info('[INFO] loading face mask detector model...')
	checkpoint = torch.load(args.checkpoint, map_location=torch.device(args.type_cpu))
	mask_detector = Classifier()
	mask_detector.load_state_dict(checkpoint['classifier'])
	preprocess_face = load_preprocess()
	net, device, cfg = get_model()

	# initialize the video stream and allow the camera sensor to warm up
	logger.info('[INFO] starting video stream...')
	vs = cv2.VideoCapture(args.input_video)
	frame_idx = 0

	# loop over the frames from the video stream
	while True:
		ret, frame = vs.read()
		if not ret:
			break

		# check frame period
		if frame_idx % args.frame_period == 0:
			frame_idx += 1
		else:
			frame_idx += 1
			continue

		# detect face box and mask
		frame = cv2.resize(frame, (1280, 720))
		dets = detect_face(net, frame, device, cfg)
		faces, locs = get_face(frame, args, dets)

		# only make a predictions if at least one face was detected
		if len(faces) > 0:
			faces = np.array(faces, dtype='float32').reshape(-1, 112, 112, 3)
			withoutMask = predict_mask(mask_detector, faces, preprocess_face, frame_idx)

		# loop over the detected face locations and their corresponding locations
		try:
			for i in range(faces.shape[0]):
				# unpack the bounding box and predictions
				(startX, startY, endX, endY) = locs[i]

				# determine the class label and color we'll use to draw the bounding box and text
				if withoutMask[i] > args.nomask_confidence: 
					label = 'NoMask'
					logger.info('Warning!!! Face NoMask Detection')
					logger.info('Score: {}'.format(withoutMask[i]))
				else:
					continue
				color = (0, 255, 0) if label == 'Mask' else (0, 0, 255)
				label = '{}: {:.2f}%'.format(label, withoutMask[i] * 100)

				# display the label and bounding box rectangle on the output frame
				cv2.putText(frame, label, (startX, startY - 10),
					cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
				cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)
		except:
			pass

		# show the output frame
		cv2.imshow('Frame', frame)
		key = cv2.waitKey(1) & 0xFF

		# if the `q` key was pressed, break from the loop
		if key == ord('q'):
			break

	# do a bit of cleanup
	vs.release()
	cv2.destroyAllWindows()


if __name__=="__main__":
    logger.info('__main__')
    main()
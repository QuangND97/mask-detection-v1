import os
import cv2
import torch
import errno
import numpy as np
from PIL import Image
import torchvision.transforms as transforms 


def get_face(frame, args, dets):
	faces = []
	locs = []
	preds = []

	(h, w) = frame.shape[:2]
	# show image
	for b in dets:
		if b[4] < args.face_confidence:
			continue
		b = list(map(int, b))
		cx = b[0]
		cy = b[1] + 12
		startX, startY, endX, endY = b[0], b[1], b[2], b[3]
		
		try:
			face = frame[startY:endY, startX:endX]
			face = cv2.resize(face, (112, 112))
			faces.append(face)
			locs.append((startX, startY, endX, endY))
		except:
			pass
	return faces, locs
    

def load_preprocess():
    return transforms.Compose([
    transforms.Resize(112),
    transforms.CenterCrop(112),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]),
    ])


def cv2toPIL(buffer, face, frame_idx, face_idx):
    """
    Convert to PIL Image
    """
    image_path = os.path.join(buffer, str(frame_idx) + '_' +  str(face_idx) + '.jpg') 
    cv2.imwrite(image_path, face)
    return Image.open(image_path), image_path


def cleanup(image_path):
    os.remove(image_path)


def maybe_create_folder(folder):
    try:
        os.makedirs(folder)
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise




#from __future__ import print_function
import os
import cv2
import time
import torch
import logging
import argparse
import numpy as np
import torch.backends.cudnn as cudnn
from .data import cfg_mnet, cfg_re50
from .models.retinaface import RetinaFace
from .utils.nms.py_cpu_nms import py_cpu_nms
from .layers.functions.prior_box import PriorBox
from .utils.box_utils import decode, decode_landm


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
args = 0
parser = argparse.ArgumentParser(description='Retinaface and MobileNetV2')
# Facebox detect Video
parser.add_argument('--trained_model', default='mask-detection-v1/weights/mobilenet0.25_Final.pth', type=str, 
    help='Trained state_dict file path to open')
parser.add_argument('--network', default='mobile0.25', 
    help='Backbone network mobile0.25 or resnet50')
parser.add_argument('--cpu', action='store_true', default=True, 
    help='Use cpu inference')
parser.add_argument('--check', default='cpu', type=str,
    help='Use cpu inference')
parser.add_argument('--confidence_threshold', default=0.02, type=float, 
    help='confidence_threshold')
parser.add_argument('--top_k', default=5000, type=int, 
    help='top_k')
parser.add_argument('--nms_threshold', default=0.4, type=float, 
    help='nms_threshold')
parser.add_argument('--keep_top_k', default=750, type=int, 
    help='keep_top_k')
parser.add_argument('-s', '--save_image', action='store_true', default=True, 
    help='show detection results')
parser.add_argument('--vis_thres', default=0.6, type=float, 
    help='visualization_threshold')
# Mask Detect Video
parser.add_argument('--face', type=str, default='face_detector',
    help='path to face detector model directory')
parser.add_argument('--checkpoint', type=str, default='mask-detection-v1/checkpoint/model_best_112.pth.tar',
    help='path to trained face mask detector model')
parser.add_argument('--buffer', type=str, default='mask-detection-v1/buffer',
    help='path to folder contains images box ectracted from face detector')
parser.add_argument('--face_confidence', type=float, default=0.95,
    help='minimum probability to filter weak detections')
parser.add_argument('--nomask_confidence', type=float, default=0.85,
    help='minimum probability to filter weak detections')
parser.add_argument('--model_name', type=str, default='MobileNetV2', 
    help='Type of CNN model')
parser.add_argument('--frame_period', type=float, default=6,
    help='frame period')
parser.add_argument('--input_video', type=str, default='mask-detection-v1/examples/IMG_0090.mp4', 
    help='video input')
parser.add_argument('--type_cpu', type=str, default='cpu', 
    help='cpu or gpu')

args = parser.parse_args()

# Check GPU or CPU
if args.check == 'gpu':
    args.cpu == False
else:
    args.cpu == True

# Setup GPU or CPU
if args.cpu == False:
    args.trained_model = 'mask-detection-v1/weights/Resnet50_Final.pth'
    args.network = 'resnet50'
    args.type_cpu = 'cuda:0'
else:
    args.trained_model = 'mask-detection-v1/weights/mobilenet0.25_Final.pth'
    args.network = 'mobile0.25'
    args.type_cpu = 'cpu'


def init():
    global args
    return args


def check_keys(model, pretrained_state_dict):
    """
    Check checkpoint model Retinaface Detect
    Return True if it have checkpoint
    """
    # Check Weight
    ckpt_keys = set(pretrained_state_dict.keys())
    model_keys = set(model.state_dict().keys())
    used_pretrained_keys = model_keys & ckpt_keys
    unused_pretrained_keys = ckpt_keys - model_keys
    missing_keys = model_keys - ckpt_keys

    # Print Error
    logger.info('Missing keys:{}'.format(len(missing_keys)))
    logger.info('Unused checkpoint keys:{}'.format(len(unused_pretrained_keys)))
    logger.info('Used keys:{}'.format(len(used_pretrained_keys)))
    assert len(used_pretrained_keys) > 0, 'load NONE from pretrained checkpoint'
    return True


def remove_prefix(state_dict, prefix):
    ''' Old style model is stored with all names of parameters sharing common prefix 'module.' '''
    logger.info('remove prefix \'{}\''.format(prefix))
    f = lambda x: x.split(prefix, 1)[-1] if x.startswith(prefix) else x
    return {f(key): value for key, value in state_dict.items()}


def load_model(model, pretrained_path, load_to_cpu):
    """
    Load model and return model
    """
    logger.info('Loading pretrained model from {}'.format(pretrained_path))
    if load_to_cpu:
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage)
    else:
        device = torch.cuda.current_device()
        pretrained_dict = torch.load(pretrained_path, map_location=lambda storage, loc: storage.cuda(device))
    if 'state_dict' in pretrained_dict.keys():
        pretrained_dict = remove_prefix(pretrained_dict['state_dict'], 'module.')
    else:
        pretrained_dict = remove_prefix(pretrained_dict, 'module.')
    check_keys(model, pretrained_dict)
    model.load_state_dict(pretrained_dict, strict=False)
    return model


def get_model():
    """
    Get MobileNetV1 or Resnet50 Backbone
    """
    torch.set_grad_enabled(False)
    cfg = None
    if args.network == 'mobile0.25':
        cfg = cfg_mnet
    elif args.network == 'resnet50':
        cfg = cfg_re50
    # net and model
    net = RetinaFace(cfg=cfg, phase = 'test')
    net = load_model(net, args.trained_model, args.cpu)
    net.eval()
    logger.info('Finished loading model!')
    # logger.info(net)
    cudnn.benchmark = True
    device = torch.device('cpu' if args.cpu else 'cuda')
    net = net.to(device)
    return net, device, cfg


def detect_face(net, image, device, cfg):
    """
    Detect face from box return
    """
    resize = 1
    img = np.float32(image)

    im_height, im_width, _ = img.shape
    scale = torch.Tensor([img.shape[1], img.shape[0], img.shape[1], img.shape[0]])
    img -= (104, 117, 123)
    img = img.transpose(2, 0, 1)
    img = torch.from_numpy(img).unsqueeze(0)
    img = img.to(device)
    scale = scale.to(device)

    #tic = time.time()
    loc, conf, landms = net(img)  # forward pass
    # logger.info('net forward time: {:.4f}'.format(time.time() - tic))

    priorbox = PriorBox(cfg, image_size=(im_height, im_width))
    priors = priorbox.forward()
    priors = priors.to(device)
    prior_data = priors.data
    boxes = decode(loc.data.squeeze(0), prior_data, cfg['variance'])
    boxes = boxes * scale / resize
    boxes = boxes.cpu().numpy()
    scores = conf.squeeze(0).data.cpu().numpy()[:, 1]
    landms = decode_landm(landms.data.squeeze(0), prior_data, cfg['variance'])
    scale1 = torch.Tensor([img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                            img.shape[3], img.shape[2], img.shape[3], img.shape[2],
                            img.shape[3], img.shape[2]])
    scale1 = scale1.to(device)
    landms = landms * scale1 / resize
    landms = landms.cpu().numpy()

    # ignore low scores
    inds = np.where(scores > args.confidence_threshold)[0]
    boxes = boxes[inds]
    landms = landms[inds]
    scores = scores[inds]

    # keep top-K before NMS
    order = scores.argsort()[::-1][:args.top_k]
    boxes = boxes[order]
    landms = landms[order]
    scores = scores[order]

    # do NMS
    dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
    keep = py_cpu_nms(dets, args.nms_threshold)
    # keep = nms(dets, args.nms_threshold,force_cpu=args.cpu)
    dets = dets[keep, :]
    landms = landms[keep]

    # keep top-K faster NMS
    dets = dets[:args.keep_top_k, :]
    landms = landms[:args.keep_top_k, :]

    dets = np.concatenate((dets, landms), axis=1)

    return dets
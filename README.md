# Installation
First we need to install:
1. Python 3.7 and pip 20.2.2
2. git and zip, unzip

# Download Engine Repositories
```bash
git clone https://QuangND97@bitbucket.org/QuangND97/mask-detection-v1.git
```

# Setup for Engine
1. Install requirements for python using pip
```bash
pip3 install -r mask-detection-v1/requirements.txt
```
2. Download all pre-train model detect faceboxes and mask
```bash
gdown https://drive.google.com/uc?id=1yJX0VIfqTrPkRykVh80jZf5cmptd2A8V
```
3. Unzip file to repositories folder

# Run file
engines input parameters
1. **nomask_confidence**: Threshold model predict that face is wearing mask or not, default = 0.85
2. **face_confidence**: Threshold model predict box that box has face or not, default = 0.95
3. **frame_period**: Number of frames passed during prediction. Exam: frame_period = 10 means that model will predict 1 frames in 10 consecutive frames. Default = 6
4. **input_video**: Link to video test, default = ./examples/IMG_0090.mp4
Exam of run in command
5. **check**: Run on cpu or gpu, default = cpu

Run command:
```bash
python3 mask-detection-v1/mask_detect.py --check cpu --nomask_confidence 0.85 --face_confidence 0.95 --frame_period 3 --input_video mask-detection-v1/examples/IMG_0090.mp4
```

If you want to use default setup
```bash
python3 mask-detection-v1/mask_detect.py
```
